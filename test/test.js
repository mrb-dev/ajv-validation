const validateRequest = require('../index');

//a schema validation of ajv
let x = {
    "title": "An object value",
    "required": [
        "clientID",
        "companyID"
    ],
    "properties": {
        "clientID": {
            "$id": "#/properties/clientID",
            "pattern": "^[0-9a-fA-F]{24}$",
            "type": "string"
        },
        "companyID": {
            "$id": "#/properties/companyID",
            "pattern": "^[0-9a-fA-F]{24}$",
            "type": "string"
        },
        "employeeID": {
            "$id": "#/properties/employeeID",
            "pattern": "^[0-9a-fA-F]{24}$",
            "type": "string"
        },
        "contractorID": {
            "$id": "#/properties/contractorID",
            "pattern": "^[0-9a-fA-F]{24}$",
            "type": "string"
        }
    },
    "if": {"required": ["employeeID"]},
    "then": {"not": {"$ref": "#/definitions/checkElse"}},
    "else": {"$ref": "#/definitions/checkElse"},
    "definitions": {
        "checkElse": {
            "anyOf": [
                {"required": ["contractorID"]}
            ]
        }
    },
    "additionalProperties": false,
    "$id": "http://example.org/example.json#",
    "type": "object",
    "$schema": "http://json-schema.org/draft-07/schema#"
}


let y = {
    "clientID": "60906ec02316277edbb21d49",
    "companyID": "6094d485353ab97fcf9499c4",
    "employeeID": "60d17fc422ee596240677bec"
}

// console.log(validateRequest);

function test(x, y) {
    return setTimeout(async function () {
        return await validateRequest(y, x)

    }, 10);
}

// let z = test(x,y)
function resolveAfter2Seconds(x, y) {
  return new Promise(resolve => {
    setTimeout(async () => {
        let test = await validateRequest(y, x)
      resolve(test);
    }, 2000);
  });
}

async function test() {
  console.log('calling');
  const result = await resolveAfter2Seconds(x, y);
  console.log(result);
  // expected output: "object"
}

test();
