const Ajv = require('ajv');

//show All error if data not valid
const ajv = new Ajv.default({
    allErrors: true,
    async: true,
    strict: false,
    // loopRequired: Infinity
}); // options can be passed, e.g. {allErrors: true}


/** Error messages */
const errors = {
	'required': 'Needs at least one Key of Object',
	'invalid-input': 'Invalid input format, should be in onject'
};

/**
 * A generic error utility function.
 * @private
 * @param {String} type The error type.
 * @returns {Error} Throws a `RangeError` with the applicable error message.
 */
const error = (type) => {
	throw new RangeError(errors[type]);
}

const handleError = (input, schemas) => {
    if (typeof input === 'string' && typeof schemas === 'string') {
		error('invalid-input');
	}

    let inputLength = sizeOfObject(input)
    if (inputLength === 0) {
        error('required')
    }
    return input;
}

/**
 * A simple function to count object length.
 * @private
 * @param {Object} type The error type.
 * @returns {Number} Number of object length.
 */
const sizeOfObject = (obj) => {
    let size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

/**
 * A function to check errors validation.
 * @param {Array} the Errors from AJV.
 * @returns {Array} A Array of values returned.
 */
const parseErrors = async (validationErrors) => {
    let errors = [];
    validationErrors.forEach(error => {
        let err = error.message
        errors.push({
            param: error.params["missingProperty"] || error.params["additionalProperty"] || error.instancePath.slice(1),
            key: error.keyword,
            message: error.message,
            property: (function() {
                return error.keyword === 'minimum' ? error.dataPath : undefined
            })()
        });
    });
    return errors;
}

/**
 * A simple function to count object length.
 * @private
 * @param {Object} Input is user input and Schemas is object where schemas is ajv requirement.
 * @returns {Object} A object of values returned.
 */
module.exports = validateRequest = async (input, schemas) => {
    try {
        let checkInput = handleError(input, schemas)
        if (!checkInput) {
            return checkInput;
        }
        let valid = ajv.validate(schemas, input);
        // console.log(valid);
        if (!valid) {
            let result = await parseErrors(ajv.errors);

            return Promise.all(result);;
        }
        // return all user's input base of schema
        return input;

    } catch (e) {
        return e;
    }
}
